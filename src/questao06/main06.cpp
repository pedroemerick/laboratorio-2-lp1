/**
 * @file	main06.cpp
 * @brief	Código fonte de teste para função de busca ternária
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#include <iostream>
#include <cstdlib>
#include "busca_ternaria.h"

using std::cout;
using std::endl;

/** 
 * @brief Função principal
 * @param argc Possui o número de argumentos com os quais a funçãoo main() foi chamada na linha de comando
 * @param argv Cada string desta matriz é um dos parâmetros da linha de comando
 */
int main (int argc, char *argv[])
{
    if (argc != 2)
    {
        cout << "Erro !!! Deve ser passado a chave que deseja verificar quando iniciar o programa. Ex.: ./buscaternaria 2" << endl;
        cout << "Inicie o programa corretamente." << endl;
    }

    int chave;  // Chave de busca
    chave = atoi (argv[1]); 
    int v[] = {2, 5, 9, 11, 13, 17, 22, 24, 33, 38, 39, 40, 45, 56, 71, 99, 110, 
                113, 132, 155, 166, 203, 211, 212, 230, 233}; // Base de dados
    int num_elem = 26;  // Tamanho da base de dados
    int ini = 0;    // Indice de inicio da base de dados
    int fim = num_elem - 1; // Indice do fim da base de dados

    if (busca_ternaria (v, ini, fim, chave) == true)
        cout << "O elemento " << chave << " faz parte do vetor." << endl;
    else
        cout << "O elemento " << chave << " nao faz parte do vetor." << endl;

    return 0;
}