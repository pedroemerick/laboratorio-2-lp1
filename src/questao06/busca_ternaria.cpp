/**
 * @file	busca_ternaria.cpp
 * @brief	Código fonte com a implementação da função de busca ternária
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#include "busca_ternaria.h"

/**
 * @brief Verifica se uma chave se encontra em uma base de dados através da busca ternária
 * @param v Base de dados
 * @param ini Indice de inicio da base de dados
 * @param fim Indice do fim da base de dados
 * @return Se a chave se encontra na base de dados
 */
bool busca_ternaria (int *v, int ini, int fim, int x)
{
    if (fim < ini)
        return false;

    int meio1 = (fim-ini)/3 + ini;
    int meio2 = 2 * (fim-ini)/3 + ini;

    if (v[meio1] == x)
        return true;
    else if (v[meio2] == x)
        return true;
    else if (v[meio1] < x)
        return busca_ternaria (v, meio1+1, fim, x);
    else if (v[meio2] > x)
        return busca_ternaria (v, ini, meio2-1, x);
    else 
        return busca_ternaria (v, meio1+1, meio2-1, x);
}