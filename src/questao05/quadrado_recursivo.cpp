/**
 * @file	quadrado_recursivo.cpp
 * @brief	Código fonte com a implementação da função que calcula o quadrado de um número de forma recursiva, 
            e da função que imprime os valores usados no cálculo do quadrado.
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#include "quadrado_recursivo.h"

/**
 * @brief Calcula o quadrado de um número de forma recursiva
 * @param num Número para cálculo do quadrado
 * @return Quadrado do número
 */
int quadrado_recursivo (int num)
{
    if (num == 0)
        return 0;
    else if (num == 1)
        return 1;
    else
        return (2*num)-1 + quadrado_recursivo (num-1);
}

/**
 * @brief Imprime os valores usados no cálculo do quadrado
 * @param num Número que foi usado no cálculo do quadrado
 */
void imprime_nums (int num)
{
    int i;

    for (i=1; i<(2*num); i+=2)
    {
        cout << i;

        if (i < (2*num)-2)
            cout << " + ";
    }
}