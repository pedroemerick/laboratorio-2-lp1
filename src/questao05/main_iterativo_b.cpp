/**
 * @file	main_iterativo_b.cpp
 * @brief	Código fonte de teste para função de cálculo do quadrado de forma iterativa e impressão dos números usados no cálculo
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#include <iostream>
#include <cstdlib>
#include "quadrado_iterativo.h"

using std::cout;
using std::endl;

/** 
 * @brief Função principal
 * @param argc Possui o número de argumentos com os quais a funçãoo main() foi chamada na linha de comando
 * @param argv Cada string desta matriz é um dos parâmetros da linha de comando
 */
int main (int argc, char* argv[])
{
    if (argc != 2)
    {
        cout << "Erro !!! Digite o número que deseja saber o quadrado pela linha de comando. Ex.: ./quadrado_recursivo 1" << endl;
        cout << "Inicie o programa corretamente." << endl;
    }
    
    int num = atoi (argv[1]); // Número para cálculo de seu quadrado

    if (num < 0)
        cout << "Erro !!! Digite apenas valores positivos ou nulo." << endl;

    cout << "quadrado (" << num << ") => ";
    imprime_nums (num);
    cout << " = " << quadrado_iterativo (num) << endl;

    return 0;
}