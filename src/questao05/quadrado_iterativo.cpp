/**
 * @file	quadrado_iterativo.cpp
 * @brief	Código fonte com a implementação da função que calcula o quadrado de um número de forma iterativa, 
            e da função que imprime os valores usados no cálculo do quadrado.
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#include "quadrado_iterativo.h"

/**
 * @brief Calcula o quadrado de um número de forma iterativa
 * @param num Número para cálculo do quadrado
 * @return Quadrado do número
 */
int quadrado_iterativo (int num)
{  
    if (num == 0)
        return 0;

    int i;
    int quadrado = 0;

    for (i=1; i<(2*num); i+=2)
    {
        quadrado += i;
    }

    return quadrado;
}

/**
 * @brief Imprime os valores usados no cálculo do quadrado
 * @param num Número que foi usado no cálculo do quadrado
 */
void imprime_nums (int num)
{
    int i;

    for (i=1; i<(2*num); i+=2)
    {
        cout << i;

        if (i < (2*num)-2)
            cout << " + ";
    }
}