/**
 * @file	decimal2binario.cpp
 * @brief	Código fonte com a implementação da função que calcula a forma binária de um número decimal
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#include "decimal2binario.h"

/**
 * @brief Calcula a forma binária de um número decimal
 * @param num_dec Número em decimal
 * @param expo Auxiliar para o cálculo do número binário
 * @return Número binário
 */
long int dec2bin (int num_dec, int expo)
{
    if (num_dec == 0)
        return 0;
    else 
        return (num_dec%2 * pow(10,expo) + dec2bin (num_dec/2, expo+1));
}