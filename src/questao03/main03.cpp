/**
 * @file	main03.cpp
 * @brief	Código fonte de teste para função que calcula a forma binária de um número decimal
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#include <iostream>
#include "decimal2binario.h"

using std::cout;
using std::cin;
using std::endl;

/** @brief Função principal */
int main ()
{
    int num_dec;    // Número decimal
    int expo = 0;   // Auxiliar para o cálculo do número binário
    int aux = 0;    // Auxiliar para condição do while

    while (aux == 0)
    {
        cout << "Digite um numero: ";
        cin >> num_dec;

        if (num_dec < 0)
            cout << "Erro !!! Digite apenas numeros positivos. Tente novamente !!!" << endl << endl;
        else
            aux = 1;
    }
    
    cout << "Representacao de " << num_dec << " na forma binaria: " << dec2bin (num_dec, expo) << endl;
    
    return 0;
}