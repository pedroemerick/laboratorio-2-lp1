/**
 * @file	main01.cpp
 * @brief	Código fonte de teste para funções que calculam sequências
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#include <iostream>
#include <cstring>
#include <cstdlib>
#include "sequencias.h"

using std::cout;
using std::endl;

/** 
 * @brief Função principal
 * @param argc Possui o número de argumentos com os quais a funçãoo main() foi chamada na linha de comando
 * @param argv Cada string desta matriz é um dos parâmetros da linha de comando
 */
int main (int argc, char *argv[])
{
    if (argc != 4)
    {
        cout << "Erro !!! Digite os valores pela linha de comando, neste modelo a seguir: ./sequencia A R 1" << endl;
        cout << "Inicie o programa corretamente." << endl;
    }   
    float n; // Número que determina até onde irá a sequência
    n = atoi (argv[3]);

    std::cout.precision(3);

    if (strcmp (argv[1], "A") == 0 && strcmp (argv[2], "R") == 0 )
        cout << "O valor da sequencia A para N = " << n << " e " << funcao_A_recursiva (n) << " (a versão recursiva foi usada)" << endl;

    if (strcmp (argv[1], "A") == 0 && strcmp (argv[2], "I") == 0)
        cout << "O valor da sequencia A para N = " << n << " e " << funcao_A_iterativa (n) << " (a versão iterativa foi usada)" << endl;

    if (strcmp (argv[1], "B") == 0 && strcmp (argv[2], "R") == 0)
        cout << "O valor da sequencia B para N = " << n << " e " << funcao_B_recursiva (n) << " (a versão recursiva foi usada)" << endl;

    if (strcmp (argv[1], "B") == 0 && strcmp (argv[2], "I") == 0)
        cout << "O valor da sequencia B para N = " << n << " e " << funcao_B_iterativa (n) << " (a versão iterativa foi usada)" << endl;

    return 0;
}
