/**
 * @file	sequencias.cpp
 * @brief	Código fonte com a implementação de funções que calculam determinadas sequências A e B
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#include "sequencias.h"

/**
 * @brief Calcula uma determinada sequência A de modo recursivo
 * @param n Número que determina até onde irá a sequência
 * @return Valor da sequência
 */
float funcao_A_recursiva (float n)
{
    if (n == 0)
        return 0;
    if (n == 1)
        return 1;
    else 
        return 1/n + (funcao_A_recursiva (n-1));
}

/**
 * @brief Calcula uma determinada sequência B de modo recursivo
 * @param n Número que determina até onde irá a sequência
 * @return Valor da sequência
 */
float funcao_B_recursiva (float n)
{
    if (n == 0)
        return 0;
    else 
        return (pow (n,2) + 1)/(n+3) + funcao_B_recursiva (n-1);
}

/**
 * @brief Calcula uma determinada sequência A de modo iterativo
 * @param n Número que determina até onde irá a sequência
 * @return Valor da sequência
 */
float funcao_A_iterativa (float n)
{
    if (n == 0)
        return 0;
    
    float soma = 0; // Valor da sequência
    float i;

    for (i=1; i<=n; i++)
    {
        soma += 1/i;
    }

    return soma;
}

/**
 * @brief Calcula uma determinada sequência B de modo iterativo
 * @param n Número que determina até onde irá a sequência
 * @return Valor da sequência
 */
float funcao_B_iterativa (float n)
{
    if (n == 0)
        return 0;
    
    float soma = 0; // Valor da sequência
    float i;

    for (i=1; i<=n; i++)
    {
        soma += (pow (i,2) + 1)/(i+3);
    }

    return soma;
}
