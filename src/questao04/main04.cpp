/**
 * @file	main04.cpp
 * @brief	Código fonte de teste para função que verifica se uma palavra é um palindromo
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#include <iostream>
#include "palindromo.h"

using std::cin;
using std::string;
using std::cout;
using std::endl;

/** @brief Função principal */
int main ()
{
    string palavra; // Palavra para verificar se é um palindromo

    cout << "Digite uma palavra: ";
    cin >> palavra;

    int tamanho = palavra.length();   // Tamanho da palavra

    int ini = 0;    // Indice do inicio da palavra
    int fim = tamanho;  // Indice do fim da palavra

    if (palindromo (palavra, ini, fim) == true)
        cout << "'" << palavra << "'" << " e um palindromo." << endl;
    else 
        cout << "'" << palavra << "'" << " nao e um palindromo." << endl;

    return 0;
}