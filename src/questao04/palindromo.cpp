/**
 * @file	palindromo.cpp
 * @brief	Código fonte com a implementação da função que verifica se uma palavra é um palindromo
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#include "palindromo.h"

/**
 * @brief Verifica se uma palavra é um palindromo
 * @param palavra Palavra para verificar se é um palindromo
 * @param ini Indice de inicio da palavra
 * @param fim Indice do fim da palavra
 * @return Se a palavra é ou não um palindromo
 */
bool palindromo (string palavra, int ini, int fim)
{
    if (tolower(palavra[ini]) != tolower(palavra[fim-1]))
        return false;
    if (fim <= ini)
        return true;
    else
        return palindromo (palavra, ini+1, fim-1);
}