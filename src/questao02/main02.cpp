/**
 * @file	main02.cpp
 * @brief	Código fonte de teste para função que calcula o MDC de dois números
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#include <iostream>
#include "funcao_mdc.h"

using std::cout;
using std::cin;
using std::endl;

/** @brief Função principal */
int main ()
{
    int num1;   // Número para calculo do MDC
    int num2;   // Número para calculo do MDC
    int aux = 0;    // Auxiliar do while

    while (aux == 0)
    {
        cout << "Digite dois numeros naturais positivos: ";
        cin >> num1;
        cin >> num2;

        if (num1 <0 || num2<0)
            cout << "Erro !!! Digite numeros positivos. Tente novamente !!!" << endl << endl;
        else if (num1 == 0 && num2 == 0)
            cout << "Erro !!! Digite numeros nao nulos. Tente novamente !!!" << endl << endl;
        else 
            aux = 1;
    }
    
    menor_maior (&num1, &num2);

    cout << "MDC (" << num1 << ", " << num2 << ") = " << mdc(num1,num2) << endl;

    return 0;
}