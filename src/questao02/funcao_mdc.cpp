/**
 * @file	funcao_mdc.cpp
 * @brief	Código fonte com a implementação da função que calcula o MDC, e da função que coloca o maior
            número no primeiro parâmetro passado e o menor no segundo parâmetro
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#include "funcao_mdc.h"

/**
 * @brief Calcula o MDC de dois números
 * @param num1 Número para o cálculo do mdc
 * @param num2 Número para o cálculo do mdc
 * @return MDC
 */
int mdc (int num1, int num2)
{
    if (num2 == 0)
        return num1;
    else if (num1 % num2 == 0)
        return num2;
    else 
        return mdc (num2, num1%num2);
}

/**
 * @brief Coloca o maior número no primeiro parâmetro passado e o menor no segundo parâmetro
 * @param num1 Número para troca
 * @param num2 Número para troca
 */
void menor_maior (int *num1, int *num2)
{
    int aux;

    if (*num2 > *num1)
    {
        aux = *num2;
        *num2 = *num1;
        *num1 = aux;
    }
}