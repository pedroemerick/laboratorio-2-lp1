LIB_DIR = ./lib
INC_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./build
BIN_DIR = ./bin
DOC_DIR = ./doc
TEST_DIR = ./test

CC = g++
CPPLAGS = -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)/questao01 -I$(INC_DIR)/questao02 -I$(INC_DIR)/questao03 -I$(INC_DIR)/questao04 -I$(INC_DIR)/questao05 -I$(INC_DIR)/questao06

RM = rm -rf
RM_TUDO = rm -fr

PROG_1 = sequencia
PROG_2 = mdc
PROG_3 = dec2bin
PROG_4 = palindromo
PROG_5_A = quadrado_recursivo
PROG_5_B = quadrado_iterativo
PROG_6 = ternaria

.PHONY: all clean debug doc doxygen

all: $(PROG_1) $(PROG_2) $(PROG_3) $(PROG_4) $(PROG_5_A) $(PROG_5_B) $(PROG_6)

debug: CFLAGS += -g -O0
debug: $(PROG_1) $(PROG_2) $(PROG_3) $(PROG_4) $(PROG_5_A) $(PROG_5_B) $(PROG_6)

#QUESTAO01
$(PROG_1): $(OBJ_DIR)/questao01/main01.o $(OBJ_DIR)/questao01/sequencias.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel $(PROG_1) criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/questao01/main01.o: $(SRC_DIR)/questao01/main01.cpp
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/questao01/sequencias.o: $(SRC_DIR)/questao01/sequencias.cpp $(INC_DIR)/questao01/sequencias.h
	$(CC) -c $(CPPLAGS) -o $@ $<

#QUESTAO02
$(PROG_2): $(OBJ_DIR)/questao02/main02.o $(OBJ_DIR)/questao02/funcao_mdc.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel $(PROG_2) criado em $(BIN_DIR)] ***"
	@echo "====================================================="
$(OBJ_DIR)/questao02/main02.o: $(SRC_DIR)/questao02/main02.cpp
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/questao02/funcao_mdc.o: $(SRC_DIR)/questao02/funcao_mdc.cpp $(INC_DIR)/questao02/funcao_mdc.h
	$(CC) -c $(CPPLAGS) -o $@ $<

#QUESTAO03
$(PROG_3): $(OBJ_DIR)/questao03/main03.o $(OBJ_DIR)/questao03/decimal2binario.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel $(PROG_3) criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/questao03/main03.o: $(SRC_DIR)/questao03/main03.cpp
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/questao03/decimal2binario.o: $(SRC_DIR)/questao03/decimal2binario.cpp $(INC_DIR)/questao03/decimal2binario.h
	$(CC) -c $(CPPLAGS) -o $@ $<

#QUESTAO04
$(PROG_4): $(OBJ_DIR)/questao04/main04.o $(OBJ_DIR)/questao04/palindromo.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel $(PROG_4) criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/questao04/main04.o: $(SRC_DIR)/questao04/main04.cpp
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/questao04/palindromo.o: $(SRC_DIR)/questao04/palindromo.cpp $(INC_DIR)/questao04/palindromo.h
	$(CC) -c $(CPPLAGS) -o $@ $<

#QUESTAO05_A
$(PROG_5_A): $(OBJ_DIR)/questao05/main_recursivo_a.o $(OBJ_DIR)/questao05/quadrado_recursivo.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel $(PROG_5_A) criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/questao05/main_recursivo_a.o: $(SRC_DIR)/questao05/main_recursivo_a.cpp
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/questao05/quadrado_recursivo.o: $(SRC_DIR)/questao05/quadrado_recursivo.cpp $(INC_DIR)/questao05/quadrado_recursivo.h
	$(CC) -c $(CPPLAGS) -o $@ $<

#QUESTAO05_B
$(PROG_5_B): $(OBJ_DIR)/questao05/main_iterativo_b.o $(OBJ_DIR)/questao05/quadrado_iterativo.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel $(PROG_5_B) criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/questao05/main_iterativo_b.o: $(SRC_DIR)/questao05/main_iterativo_b.cpp
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/questao05/quadrado_iterativo.o: $(SRC_DIR)/questao05/quadrado_iterativo.cpp $(INC_DIR)/questao05/quadrado_iterativo.h
	$(CC) -c $(CPPLAGS) -o $@ $<

#QUESTAO06
$(PROG_6): $(OBJ_DIR)/questao06/main06.o $(OBJ_DIR)/questao06/busca_ternaria.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel $(PROG_6) criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/questao06/main06.o: $(SRC_DIR)/questao06/main06.cpp
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/questao06/busca_ternaria.o: $(SRC_DIR)/questao06/busca_ternaria.cpp $(INC_DIR)/questao06/busca_ternaria.h
	$(CC) -c $(CPPLAGS) -o $@ $<

doxygen:
	doxygen -g

doc:
	@echo "====================================================="
	@echo "Limpando pasta $(DOC_DIR)"
	@echo "====================================================="
	$(RM_TUDO) $(DOC_DIR)/*
	@echo "====================================================="
	@echo "Gerando nova documentação na pasta $(DOC_DIR)"
	@echo "====================================================="
	doxygen Doxyfile

clean:
	@echo "====================================================="
	@echo "Limpando pasta $(BIN_DIR) e $(OBJ_DIR)"
	@echo "====================================================="
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/questao01/*
	$(RM) $(OBJ_DIR)/questao02/*
	$(RM) $(OBJ_DIR)/questao03/*
	$(RM) $(OBJ_DIR)/questao04/*
	$(RM) $(OBJ_DIR)/questao05/*
	$(RM) $(OBJ_DIR)/questao06/*














