/**
 * @file	quadrado_iterativo.h
 * @brief	Declaração dos prototipos da função que calcula o quadrado de um número de forma iterativa, 
            e da função que imprime os valores usados no cálculo do quadrado.
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#ifndef QUADRADO_ITE_H
#define QUADRADO_ITE_H

#include <iostream>
using std::cout;

/**
 * @brief Calcula o quadrado de um número de forma iterativa
 * @param num Número para cálculo do quadrado
 * @return Quadrado do número
 */
int quadrado_iterativo (int num);

/**
 * @brief Imprime os valores usados no cálculo do quadrado
 * @param num Número que foi usado no cálculo do quadrado
 */
void imprime_nums (int num);

#endif