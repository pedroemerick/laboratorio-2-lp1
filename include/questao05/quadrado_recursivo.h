/**
 * @file	quadrado_recursivo.h
 * @brief	Declaração dos prototipos da função que calcula o quadrado de um número de forma recursiva, 
            e da função que imprime os valores usados no cálculo do quadrado.
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#ifndef QUADRADO_REC_H
#define QUADRADO_REC_H

#include <iostream>
using std::cout;

/**
 * @brief Calcula o quadrado de um número de forma recursiva
 * @param num Número para cálculo do quadrado
 * @return Quadrado do número
 */
int quadrado_recursivo (int num);

/**
 * @brief Imprime os valores usados no cálculo do quadrado
 * @param num Número que foi usado no cálculo do quadrado
 */
void imprime_nums (int num);

#endif