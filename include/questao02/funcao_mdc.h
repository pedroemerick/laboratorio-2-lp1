/**
 * @file	funcao_mdc.h
 * @brief	Declaração dos prototipos da função que calcula o MDC, e da função que coloca o maior
            número no primeiro parâmetro passado e o menor no segundo parâmetro
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#ifndef FUNCAO_MDC_H
#define FUNCAO_MDC_H

/**
 * @brief Calcula o MDC de dois números
 * @param num1 Número para o cálculo do mdc
 * @param num2 Número para o cálculo do mdc
 * @return MDC
 */
int mdc (int num1, int num2);

/**
 * @brief Coloca o maior número no primeiro parâmetro passado e o menor no segundo parâmetro
 * @param num1 Número para troca
 * @param num2 Número para troca
 */
void menor_maior (int *num1, int *num2);

#endif