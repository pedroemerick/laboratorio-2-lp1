/**
 * @file	decimal2binario.h
 * @brief	Declaração dos prototipos da função que calcula a forma binária de um número decimal
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#ifndef DEC_2_IN_H
#define DEC_2_BIN_H

#include <cmath>
using std::pow;

/**
 * @brief Calcula a forma binária de um número decimal
 * @param num_dec Número em decimal
 * @param expo Auxiliar para o cálculo do número binário
 * @return Número binário
 */
long int dec2bin (int num_dec, int expo);

#endif