/**
 * @file	palindromo.h
 * @brief	Declaração dos prototipos da função que verifica se uma palavra é um palindromo
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#ifndef PALINDROMO_H
#define PALINDROMO_H

#include <cctype>
#include <iostream>
using std::string;
using std::tolower;

/**
 * @brief Verifica se uma palavra é um palindromo
 * @param palavra Palavra para verificar se é um palindromo
 * @param ini Indice de inicio da palavra
 * @param fim Indice do fim da palavra
 * @return Se a palavra é ou não um palindromo
 */
bool palindromo (string palavra, int ini, int fim);

#endif