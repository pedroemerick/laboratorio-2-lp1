/**
 * @file	busca_ternaria.h
 * @brief	Declaração dos prototipos da função de busca ternária
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#ifndef BUSCA_TERNARIA_H
#define BUSCA_TERNARIA_H

/**
 * @brief Verifica se uma chave se encontra em uma base de dados através da busca ternária
 * @param v Base de dados
 * @param ini Indice de inicio da base de dados
 * @param fim Indice do fim da base de dados
 * @return Se a chave se encontra na base de dados
 */
bool busca_ternaria (int *v, int ini, int fim, int x);

#endif