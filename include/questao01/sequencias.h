/**
 * @file	sequencias.h
 * @brief	Declaração dos prototipos de funções que calculam determinadas sequências A e B
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#ifndef SEQUENCIAS_H
#define SEQUENCIAS_H

#include <cmath>
using std::pow;

/**
 * @brief Calcula uma determinada sequência A de modo recursivo
 * @param n Número que determina até onde irá a sequência
 * @return Valor da sequência
 */
float funcao_A_recursiva (float n);

/**
 * @brief Calcula uma determinada sequência B de modo recursivo
 * @param n Número que determina até onde irá a sequência
 * @return Valor da sequência
 */
float funcao_B_recursiva (float n);

/**
 * @brief Calcula uma determinada sequência A de modo iterativo
 * @param n Número que determina até onde irá a sequência
 * @return Valor da sequência
 */
float funcao_A_iterativa (float n);

/**
 * @brief Calcula uma determinada sequência B de modo iterativo
 * @param n Número que determina até onde irá a sequência
 * @return Valor da sequência
 */
float funcao_B_iterativa (float n);

#endif